package by.gsu.igi.lectures.lecture04;

/**
 * This class represents any <b>dog</b> in the world.
 * See the {@link by.gsu.igi.lectures.lecture01.NewHelloWorld#main(String[])}
 *
 * @author Evgeniy Myslovets
 * @version 21213
 * @see by.gsu.igi.lectures.lecture02.StaticDemo
 * @since version 123
 */
public class Dog extends Animal {

    String name;

    /**
     * Creates a dog with a name
     *
     * @param name specifies the new name
     */
    public Dog(String name) {
        this.name = name;
    }

    public Dog rename(String newName) {
        return new Dog(newName);
    }

    public String toString() {
        return "Dog named " + name;
    }

    @Override
    public void makeSound() {
        System.out.println("Woof!");
    }

    @Override
    public void touch() {
        System.out.println("Rrrrr");
    }
}
