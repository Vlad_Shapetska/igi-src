package by.gsu.igi.lectures.lecture08;


import java.io.*;
import java.util.Arrays;

import by.gsu.igi.lectures.lecture05.Dog;

/**
 * @author Evgeniy Myslovets
 */
public class SerializationDemo {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream objectOut = new ObjectOutputStream(out);

        objectOut.writeObject(new Dog("Pluto"));
        objectOut.flush();

        byte[] bytes = out.toByteArray();
        System.out.println(Arrays.toString(bytes));
        System.out.println(new String(bytes));

        ByteArrayInputStream in = new ByteArrayInputStream(bytes);
        ObjectInputStream objectIn = new ObjectInputStream(in);
        Object data = objectIn.readObject();
        System.out.println(data);
    }
}
