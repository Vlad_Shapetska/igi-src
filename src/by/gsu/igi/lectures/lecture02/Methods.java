package by.gsu.igi.lectures.lecture02;

import java.math.BigInteger;

public class Methods {

    public static class NumberHolder {
        int n;
    }

    public static void doStuff(int n) {
        n = n + 1;
    }

    public static void doStuff(BigInteger n) {
        n = n.add(BigInteger.ONE);
    }

    public static void doStuff(NumberHolder holder) {
        // holder = new NumberHolder();
        holder.n++;
    }

    public static void main(String[] args) {
        int n = 0;
        doStuff(n);
        System.out.println(n);

        BigInteger big = BigInteger.ZERO;
        doStuff(big);
        System.out.println(big);

        NumberHolder holder = new NumberHolder();
        doStuff(holder);
        System.out.println(holder.n);

        Dog dog = new Dog();
        dog.name = "Puppy";
        System.out.println(dog);
    }
}
